from Bio import SeqIO
import collections
import os
import argparse

bc_species = {"ATCG":"Bt7330",
"TCGA":"Bt7330",
"ACGT":"Bvulgatus",
"TACG":"Bvulgatus",
"CGAT":"Bovatus",
"GATC":"Bovatus",
"GTAC":"Buniformis",
"CGTA":"Buniformis",
"TCAG":"BWH2",
"ACTG":"BWH2",
"TCGT":"BtVPI"}

bc_direction = {"ATCG":"Forward",
"TCGA":"Reverse",
"ACGT":"Forward",
"TACG":"Reverse",
"CGAT":"Forward",
"GATC":"Reverse",
"GTAC":"Forward",
"CGTA":"Reverse",
"TCAG":"Forward",
"ACTG":"Reverse",
"TCGT":"Both"}

def distance(a, b):
    """
    INPUT: two n-sequences
    OUTPUT: a mismatch score for index 0:n-1 and 1:n
    """
    score = [i!=j for i,j in zip(a,b)]
    return(sum(score[:-1]),sum(score[1:]))

def tn_match(seq,n_mis):
    """
    INPUT: sequence and miss tolerance
    OUTPUT: Tuple with (number of mismatches, match index)
    """
    #check distance from Tn Sequence?
    p1,p2 = distance(seq, 'ACAGGTTG')
    if p1<=n_mis:
    #first index matches
        return (1,p1)
    elif p2<=n_mis:
    #second index matches
        return (2,p2)
    else:
    #no match to the Tn Sequence
        return (0,0)

def tn_exact(seq):
    """
    INPUT: sequence and miss tolerance
    OUTPUT: match index and number of mismatches
    should be faster than iterating python code
    """
    if seq[:-1]=='ACAGGTTG':
        return 1
    elif seq[1:]=='ACAGGTTG':
        return 2
    else:
        return 0


def parse_map(mapfile):
    """
    INPUT: mapping file arranged barcode\tsample\tcomma-delim list of species abbreviations
    OUTPUT: dictionary of bc:sample, dictionary of sample:species list, set of all barcodes
    """
    speciesmap = {}
    samplemap ={}
    with open(mapfile, 'r') as f:
        for lin in f.readlines():
            pline = lin.strip().split('\t')
            speciesmap[pline[1]]=pline[2].split(',')
            samplemap[pline[0]]=pline[1]
    barcodes = {k for k in samplemap.keys()}
    return((samplemap, speciesmap, barcodes))


def inseq_process(fastqfile, indexfile, n_mis, fasdir, mapping_file):
    """
    Input: files containing fastq read1 and index, miss tolerance, directory to save fasta's, sample mapping file
    Output: separated fastas for each sample-species pair
    """
    sample, species, bcs = parse_map(mapping_file)
    #pick tn matching fxn
    if n_mis > 0:
        tn_fxn = tn_match
    else:
        #why the other var?
        tn_fxn = lambda x,y: (tn_exact(x), 0)

    #counter variables to provide summary stats
    bcCount = collections.Counter()
    bcmatchCounter = collections.Counter()
    tnMatchCount = collections.Counter()
    #dictionary of fasta files to write to
    filedict = dict()
    # make fastq for each species instead
    totalspecies = list({i for v in species.values() for i in v})
    otherfiles = ['nonTN', 'nonBC']
    #make a file for each sample-species
    for v in totalspecies+otherfiles:
        filname = fasdir+'/'+v+'.fas'
        #may fail if too many files opened
        try:
            filedict[v] = open(filname,'w',100000)
        except:
            print('Warning Failed to open: '+filname)
    #### make iterator over read files
    read_iterator = SeqIO.parse(fastqfile, "fastq")
    index_iterator = SeqIO.parse(indexfile, "fastq")
    #zip iterators together and process
    for num, (read_it, index_it) in enumerate(zip(read_iterator,index_iterator)):
        # check if seq id matches
        if read_it.id != index_it.id:
            print('files not synced')
            break
        # possibly check the quality of the read?
        #if quality_check(read, seq):
            #continue
        bc = str(read_it.seq[0:7])
        #check if sample barcode is in the set
        if bc in bcs:
            #count a sample-bc match
            bcCount[bc]+=1
            #find tn matches 16:25 original range (code cuts out 7 bp barcode), new range
            pos = tn_fxn(str(read_it.seq[23:32]), n_mis)
            #if there is a tn
            if pos[0] > 0:
                tnMatchCount[pos]+=1#count transposon matches (may need to concat match pos and quality)
                inner_bc = str(index_it.seq[1:5]) #take species bc
                bcmatchCounter[bc+inner_bc]+=1
                #take non-bc sequence
                if bc_species.get(inner_bc, 0) in species[sample[bc]]:
                    trimmedseq = str(read_it.seq[7:22+pos[0]])#trimmed seq
                    #bcsortedseqs-type printing of fas files
                    filedict[bc_species[inner_bc]].write('>'+sample[bc]+'\n'+trimmedseq+'\n')

            else:
                #this goes into nonTN
                filedict['nonTN'].write('>'+sample[bc]+':'+bc+'_'+str(num)+'\n'+str(read_it.seq)+'\n')
        else:
            #this goes in the non bc file
            filedict['nonBC'].write('>seq'+str(num)+'\n'+str(read_it.seq)+'\n')
        # close all files
    for fil in filedict.values():
        fil.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Multitaxon INSeq pipeline")
    parser.add_argument("-m","--mapping", type=str, required=True, help="Mapping file")
    parser.add_argument("-f","--fastq", type=str, nargs='+', required=True, help="Raw reads file")
    parser.add_argument("-i","--index", type=str, nargs='+', required=True, help="Index reads file")
    parser.add_argument("-d","--fasdir", type=str, required=True, help="Fastq Directory")
    parser.add_argument("-n","--nmis", type=int,required=False, default=0, help="Number of mismatches allowed for transposon sequence")
    # parser.add_argument("-p","--mp", type=int, required=False,default=0, help="Number of threads")
    args = parser.parse_args()

    if not os.path.exists(args.fasdir):
        os.makedirs(args.fasdir)
    inseq_process(args.fastq[0], args.index[0],args.nmis, args.fasdir,  args.mapping)
    # if(args.mp>0)
    # with Pool(processes=args.mp) as pool:
    #     pool.map(inseq_process, zip(args.fastq, args.index))
