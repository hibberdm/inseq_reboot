"""
Zach Beller, 2018
Script to take INSEQ2 output (pooled samples)
Format: (sorted sample, location)
count   sample  reverse(T/F)    location


"""

import os
import argparse
import numpy as np
import pickle
def normalized_proccessed_filter(input_file,output_file,prefilter_tot = 2, filter_tot = 5, filter_lr = -1):
    """
    INPUT: output from process_bowtie_output.pl
    OUTPUT: filtered/normalized output
    """
    #open file specified in input
    with open(input_file, 'r') as inf:
        #initialize a list for the chr and location then another for total count data
        filtlist = []
        filtarray = []
        #iterate over lines
        for lin in inf.readlines():
            #strip endline and split by tabs
            linlist = lin.strip().split('\t')
            #Maybe filter by the number of forward/reverse reads? also filter by total reads  and
            if int(linlist[2])>filter_lr and int(linlist[3])>filter_lr and int(linlist[4])>prefilter_tot:
                #append to output lists
                filtlist.append('\t'.join(linlist[0:2]))
                filtarray.append(float(linlist[4]))
        with open(output_file, 'w') as onf:
            onf.writelines("%s\t%f\n" % (k,v) for k, v in zip(filtlist, filtarray) if v > filter_tot)
    #print total reads and coverage for file
    print(input_file,sum(filtarray),np.mean(filtarray))


def map_genes(input_file,out_file,path,ptt_dir, percent = 0.8, return_complement = True):
    org = input_file.split("_")[-1]
    ptt_file = next(ptt for ptt in os.listdir(ptt_dir) if ptt.find(org)>0)
    #load ptt list
    with open(ptt_file, 'rb') as inf:
        ptt = pickle.load(inf)
    #read in parsed bowtie output
    df1 =pd.read_csv(input_file,sep="\t", names=['chr', 'locus', 'reads'])
    #calculate length and use strand info to appropriately cut the orf frames percent-wise
    length = abs(df.loc[:,'start']-df.loc[:,'end'])
    df.loc[:,'end_trim'] = df.loc[:,'end']+(df.loc[:,'strand']=='+')*(percent-1)*length
    df.loc[:,'start_trim'] = df.loc[:,'start']+(df.loc[:,'strand']=='-')*(1-percent)*length

    #make two vectors of insertions into the starts and ends of reading frames
    ss1 = np.searchsorted(df.loc[:,'start_trim'],df1.loc[:,'locus'], side='right')
    ss2 = np.searchsorted(df.loc[:,'end_trim'],df1.loc[:,'locus'], side='left')
    #iterate through insertion indices (need to shift starts back) and report those that match (are inside of the start and end of the same orf)
    df1.loc[:,'locus_tag'] = [df.iloc[s1,:].loc['locus_tag'] if s1 == s2 and s1!=len(df)  else '' for s1,s2 in zip(ss1-1, ss2)]
    if return_complement:
        #take part not included in trimmed gene body and perform same operation
        df.loc[:,'end_c'] = df.loc[:,'end']-(df.loc[:,'strand']=='-')*percent*length
        df.loc[:,'start_c'] = df.loc[:,'start']+(df.loc[:,'strand']=='+')*percent*length
        #find intersecting bins
        ss1 = np.searchsorted(df.loc[:,'start_c'],df1.loc[:,'locus'], side='right')
        ss2 = np.searchsorted(df.loc[:,'end_c'],df1.loc[:,'locus'], side='left')
        df1.loc[:,'complement_locus_tag'] = [df.iloc[s1,:].loc['locus_tag'] if s1 == s2 and s1!=len(df)  else '' for s1,s2 in zip(ss1-1, ss2)]
    #sort by location
    df1.sort_values('locus', inplace=True)
    #write to file? or hdf5??
    df1.write_csv(out_file)


def parse_ptt(ptt_file):
    with open(ptt_file, 'r') as inf:
        while True:
            header = inf.readline()
            if header == 'Location\tStrand\tLength\tPID\tGene\tSynonym\tCode\tCOG\tProduct\n':
                break
        ptt = [lin.split('\t')[0:6] for lin in inf.readlines()]
    ptt = [(*[int(i) for i in p[0].split('..')],p[1], p[5]) for p in ptt]
    df = pd.DataFrame(ptt, columns = ['start','end', 'strand','locus_tag'])
    with open('ptts/'+ptt_file+'.pkl', 'wb') as out:
        pickle.dump(ptt,out)



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Process process_bowtie_output.pl output")
    parser.add_argument("-i","--input", type=str, required=True, help="Input tsv")
    parser.add_argument("-o","--output", type=str, required=True, help="Output tsv")
    parser.add_argument("-lr","--lrfilter", type=int, required=False, help="L/R Read Filter")
    parser.add_argument("-t","--totfilter", type=int, required=False,default = 5, help="Total Read Filter")
    parser.add_argument("-p","--ptt", type=bool, required=False,default = False, help="Do you need ptt pickles?")
    args = parser.parse_args()
    org_list = #somehow parse input
    #Make pickled ptt files
    if args.ptt:
        ptt_list = list(itertools.chain(*[[ f for f in os.listdir(path+"/indexes/"+org) if f.endswith('.ptt')and not f.startswith('.')] for org in org_list]))
        for ptt in ptt_list:
            parse_ptt(ptt)
    if args.norm == 'cpm':
        normalized_proccessed_filter(args.input,args.output, norm_fxn=cpm_norm)
    else:
        normalized_proccessed_filter(args.input,args.output)
